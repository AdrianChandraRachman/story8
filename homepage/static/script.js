$(document).ready(() => {
    start("Hey")
})

$("#keyword").keyup(
    function() {
        var keys = $("#keyword").val();
        start(keys);
    }
)

function start(keys) {
    $.ajax({
        url : "/"+keys+"/",
        success: function(hasil) {
            console.log(hasil.items);
            var obj_hasil = $("#hasil");
            obj_hasil.empty();
            obj_hasil.append(
                '<tr>' +
                    '<th>Title</th>' +
                    '<th>Author</th>' +
                    '<th>Publisher</th>' +
                '</tr>'
            );
            for ( i = 0; i < hasil.items.length; i++) {
                var stuff = hasil.items[i]
                var tmp_title = stuff.volumeInfo.title;
                var tmp_author = stuff.volumeInfo.authors;
                var tmp_publisher = stuff.volumeInfo.publisher;
                obj_hasil.append(
                    '<tr>' +
                        '<td>' + tmp_title + '</td>' +
                        '<td>' + tmp_author + '</td>' +
                        '<td>' + tmp_publisher + '</td>' +
                    '</tr>'
                );
            }
        }
    })
}
