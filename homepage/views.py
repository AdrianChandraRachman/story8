from django.shortcuts import render
from django.http.response import JsonResponse
import urllib.request, json

# Create your views here.
def index(request):
    return render(request, "index.html")


def api(request, title):
    link = urllib.parse.quote("https://www.googleapis.com/books/v1/volumes?q=" + title, "/:?=")
    with urllib.request.urlopen(link) as url:
        data = json.loads(url.read().decode())
    return JsonResponse(data)
